
.. Design Thinking slides file, created by
   hieroglyph-quickstart on Thu Aug 10 12:44:05 2017.


Design Thinking
~~~~~~~~~~~~~~~

Improve everything you do, by slowing down, and thinking.

whoami
~~~~~~

Igor Galić — Co-Founder of things

igalic — Infrastructure & Open Source

@hirojin - Disappointing Expectations

.. note:: HI, my name is Igor

          I'm a co-founder of various open source projects (Vox Pupuli & FruitfulJS). I'm active (or, have been) on many more than that (Apache httpd & trafficserver), Puppet (modules), and recently, iocage.

          I am also the cofounder of my company, where the general theme of Infrastructure & Open Source continues.

whoami
======

✨🌈queer✨🌈.

.. figure:: _static/genders-shoes.png
   :align: right
   :target: https://twitter.com/hirojin/status/646652627581829120
   :scale: 75%

.. note:: Daenny Sluijters who founded Voxpupuli with me, also started the fine tradition at PuppetConf, **proudly** proclaim, that he is gay.

    despite, or perhaps **because** of appearances I find important to mention that I'm queer, because

    - i am white
    - male looking (passing?)
    - english speaking

Bad Design
==========

Design is hard

.. figure:: _static/teapot-for-masochists.jpeg
   :align: right
   
.. note:: This is beautiful teapot is the prime example of *how* hard design can be.

   For the longest time, I didn't know quite know that something was missing.
   Until i started going to different conferences and listening to different people's problems *and* solutions.

Bad Solutions
=============

Copying Success

.. figure:: _static/A_Pattern_Language.jpg
   :align: right
   
.. note:: A Pettern language offers a beautiful set of solutions

          But the way they were adapted into IT was a little lacking.
          A pattern language is such perfect example: most of the patterns it describes
          are from essential human needs, or arise from habits.
          The "languages" it then describes are formed to optimally fit into the environment and allow for a natural flow.

          The patterns in programming languages, however, always arise from some
          concepts in the language lacking. The factory pattern is prime example
          for this.

Consistency
===========

.. figure:: _static/ioc-inconsistencies.png
   :target: https://github.com/iocage/libiocage/issues/233

.. note:: I'm currently working on a project that's the third rewrite.

      as you can imagine, we've got quite some bagage to drag behind ourselves


Security
========

.. figure:: _static/gpg--help.png
   :target: https://sealas.at/
      
.. note:: perhaps the most glaring case where we *all* notice a lack of design is security

It's your Turn!
~~~~~~~~~~~~~~~

What are your experience with a lack of design?


Links & References
~~~~~~~~~~~~~~~~~~

Background: https://www.flickr.com/photos/thefreerangekid/19814791259/
